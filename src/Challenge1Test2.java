import java.util.Scanner;

public class Challenge1Test2 {
    public static Scanner scanner = new Scanner(System.in);


    public static void main(String[] args) {
        viewMenuUtama();
    }


    public static String Input(String input) {
        System.out.print(input + ": ");
        String data = scanner.nextLine();
        return data;
    }

    public static void viewMenuUtama() {
        System.out.println("=====================================");
        System.out.println("KALKULATOR PENGHTIUNG LUAS DAN VOLUME");
        System.out.println("=====================================");
        System.out.println("Menu");
        System.out.println("1. Hitung Luas");
        System.out.println("2. Hitung Volume");
        System.out.println("0. Tutup Aplikasi");
        String pilihan = Input("Pilihan anda");
        if (pilihan.equals("1")) {
            viewMenuLuas();
        } else if (pilihan.equals("2")) {
            viewMenuVolume();
        } else if (pilihan.equals("0")) {
            System.exit(0);
        } else {
            System.out.println("Pilihan invalid!");
            viewMenuUtama();
        }
    }

    //BAGIAN LUAS
    public static void viewMenuLuas() {
        System.out.println("===============================");
        System.out.println("Pilih bidang yang akan dihitung");
        System.out.println("===============================");
        System.out.println("1. Persegi");
        System.out.println("2. Lingkaran");
        System.out.println("3. Segitiga");
        System.out.println("4. Persegi panjang");
        System.out.println("0. Kembali ke menu sebelumnya");
        String pilihan = Input("Pilihan anda");
        if (pilihan.equals("1")) {
            viewMenuPersegi();
        } else if (pilihan.equals("2")) {
            viewMenuLingkaran();
        } else if (pilihan.equals("3")) {
            viewMenuSegitiga();
        } else if (pilihan.equals("4")) {
            viewMenuPersegiPanjang();
        } else if (pilihan.equals("0")) {
            viewMenuUtama();
        } else {
            System.out.println("Pilihan Invalid");
            viewMenuLuas();
        }

    }

    public static void viewMenuPersegi() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("==========================");
        System.out.println("Anda memilih menu persegi");
        System.out.println("=========================");
        System.out.print("Masukkan sisi : ");
        float LuasPersegi;
        float s = scanner.nextFloat();
        LuasPersegi = s * s;
        System.out.println("Luas dari persegi adalah " + LuasPersegi);
        System.out.print("Tekan enter untuk kembali ke menu utama");
        Scanner input = new Scanner(System.in);
        input.nextLine();
        viewMenuUtama();
    }

    public static void viewMenuLingkaran() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("==========================");
        System.out.println("Anda memilih menu lingkaran");
        System.out.println("=========================");
        System.out.print("Masukkan jari-jari : ");
        float LuasLingkaran;
        float r = scanner.nextFloat();
        LuasLingkaran = 3.14f * r * r;
        System.out.println("Luas dari lingkaran adalah " + LuasLingkaran);
        System.out.print("Tekan enter untuk kembali ke menu utama");
        Scanner input = new Scanner(System.in);
        input.nextLine();
        viewMenuUtama();
    }

    public static void viewMenuSegitiga() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("==========================");
        System.out.println("Anda memilih menu segitiga");
        System.out.println("=========================");
        System.out.print("Masukkan alas : ");
        float LuasSegitiga;
        float a = scanner.nextFloat();
        System.out.print("Masukkan tinggi : ");
        float t = scanner.nextFloat();
        LuasSegitiga = 0.5f * a * t;
        System.out.println("Luas dari segitiga adalah " + LuasSegitiga);
        System.out.print("Tekan enter untuk kembali ke menu utama");
        Scanner input = new Scanner(System.in);
        input.nextLine();
        viewMenuUtama();
    }

    public static void viewMenuPersegiPanjang() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("==========================");
        System.out.println("Anda memilih menu persegi panjang");
        System.out.println("=========================");
        System.out.print("Masukkan panjang : ");
        float LuasPersegiPanjang;
        float p = scanner.nextFloat();
        System.out.print("Masukkan lebar : ");
        float l = scanner.nextFloat();
        LuasPersegiPanjang = p * l;
        System.out.println("Luas dari persegi panjang adalah " + LuasPersegiPanjang);
        System.out.print("Tekan enter untuk kembali ke menu utama");
        Scanner input = new Scanner(System.in);
        input.nextLine();
        viewMenuUtama();
    }

    //BAGIAN VOLUME
    public static void viewMenuVolume() {
        System.out.println("===============================");
        System.out.println("Pilih bangun yang akan dihitung");
        System.out.println("===============================");
        System.out.println("1. Kubus");
        System.out.println("2. Balok");
        System.out.println("3. Tabung");
        System.out.println("0. Kembali ke menu sebelumnya");
        String pilihan = Input("Pilihan anda");
        if (pilihan.equals("1")) {
            viewMenuKubus();
        } else if (pilihan.equals("2")) {
            viewMenuBalok();
        } else if (pilihan.equals("3")) {
            viewMenuTabung();
        } else if (pilihan.equals("0")) {
            viewMenuUtama();
        } else {
            System.out.println("Pilihan Invalid");
            viewMenuVolume();
        }
    }

    public static void viewMenuKubus() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("==========================");
        System.out.println("Anda memilih menu Kubus");
        System.out.println("=========================");
        System.out.print("Masukkan sisi : ");
        float VolumeKubus;
        float s = scanner.nextFloat();
        VolumeKubus = s * s * s;
        System.out.println("Volume dari kubus adalah " + VolumeKubus);
        System.out.print("Tekan enter untuk kembali ke menu utama");
        Scanner input = new Scanner(System.in);
        input.nextLine();
        viewMenuUtama();
    }

    public static void viewMenuBalok() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("==========================");
        System.out.println("Anda memilih menu balok");
        System.out.println("=========================");
        System.out.print("Masukkan panjang : ");
        float VolumeBalok;
        float p = scanner.nextFloat();
        System.out.print("Masukkan lebar : ");
        float l = scanner.nextFloat();
        System.out.print("Masukkan tinggi : ");
        float t = scanner.nextFloat();
        VolumeBalok = p * l * t;
        System.out.println("Volume dari balok adalah " + VolumeBalok);
        System.out.print("Tekan enter untuk kembali ke menu utama");
        Scanner input = new Scanner(System.in);
        input.nextLine();
        viewMenuUtama();
    }

    public static void viewMenuTabung() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("==========================");
        System.out.println("Anda memilih menu tabung");
        System.out.println("=========================");
        System.out.print("Masukkan alas : ");
        float VolumeTabung;
        float a = scanner.nextFloat();
        System.out.print("Masukkan tinggi : ");
        float t = scanner.nextFloat();
        VolumeTabung = 0.5f * a * t;
        System.out.println("Volume dari tabung adalah " + VolumeTabung);
        System.out.print("Tekan enter untuk kembali ke menu utama");
        Scanner input = new Scanner(System.in);
        input.nextLine();
        viewMenuUtama();
    }

}
